# LoRa HAT Raspberry Pi RF95 Gateway

This repository contains the way we can use to operate LoRA HAT (Hardware Attached on Top) with Raspberry Pi (i am using RPI3B) as a Gateway. In this project, this lora sends a data to our backend service called Mesosfer.

# REQUIREMENTS
Hardware Requirements

1.	Raspberry Pi 3 Model B
2.	Dragino Lora HAT

Software Requirements

1.	Library BCM2835
	-	wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.52.tar.gz
	-	tar zxvf bcm2835
	-	cd bcm2835*
	-	./configure
	-	make
	-	sudo make check
	-	sudo make install
2.	CFFI
	-	pip install cffi
3.	Radiohead Library
	-	https://github.com/hallard/RadioHead (already included in this repo)

# FEATURES
1.	Bandwidth configuration
	To set the bandwidth use this line code.
	rf95.setSignalBandwith(<bw>);
	All possible values are listed in pyRadioHeadRF95.py 

2.	Spreading factor configuration
	To set the spreading factor use this line code.
	rf95.setSpreadingFactor(<sf>);
	All possible values are listed in pyRadioHeadRF95.py 

3.	Center frequency configuration
	To set the center frequency use this line code.
	rf95.setFrequency(<cf>);
	All possible values are listed in pyRadioHeadRF95.py 

4.	Coding rate configuration
	To set the coding rate use this line code.
	rf95.setCodingRate4(<cr>);
	All possible values are listed in pyRadioHeadRF95.py 

5.	Transmission power configuration
	To set the transmission power that will be used, this line code.
	rf95.setTxPower(<dBm>, <useRFO>);
	All possible values are listed in pyRadioHeadRF95.py 

6.	Multicast and broadcast transmission data
	This repo can configure LoRa HAT to act as a broadcast transmission data and multicast as well. rf_reliable_server.py is for multicast, and rf_server.py is for broadcast. The difference between multicast and broadcast is where the data will be sent, broadcast will transmit data to any possible device in network area. Multicast will only transmit and receive from particular device that suit with the address.
	MESOSFER CONFIGURATION

# COMPILE AND RUN THE PROGRAM
Before run this program, you have to compile it first. Back to the parent directory where the Makefile belongs, then run this command in the terminal.

	-	make
Afterwards, you can run the program.

	-	sudo ./examples/rf_server.py (for broadcast system)
	-	sudo ./examples/rf_reliable_server.py (for multicast system)
# REFERENCES
-	https://github.com/hallard/RadioHead
-	https://github.com/exmorse/pyRadioHeadRF95
-	https://wiki.dragino.com/index.php?title=Lora/GPS_HAT




