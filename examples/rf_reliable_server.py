#!/usr/bin/python

import sys, os
import paho.mqtt.publish as publish
# Add path to pyRadioHeadRF95 module
sys.path.append(os.path.dirname(__file__) + "/../")

import pyRadioHeadRF95 as radio
import time

CLIENT_ADDRESS = 1
SERVER_ADDRESS = 2

rf95 = radio.RF95()

rf95.managerInit(SERVER_ADDRESS)

rf95.setFrequency(868)
rf95.setTxPower(14, False)
#rf95.setSignalBandwidth(rf95.Bandwidth500KHZ)
#rf95.setSpreadingFactor(rf95.SpreadingFactor12)
#rf95.setCodingRate4(rf95.CodingRate4_8)

numb = 0
topic = "device/lora/Lora Gateway"

print "StartUp Done!"
print "Receiving..."

while True:
	if rf95.available():
		numb+=1
		print "Available"
		(msg, l, source) = rf95.recvfromAck()
		print "Data #" + str(numb) + "\nReceived: " + msg + " (" + str(l) + ") from: " + str(source)
		if(source==1):
        		publish.single(topic, '{"appId":"pnsyDWDNrQ", "schemaId":"HZ6aV3ha7W", "bucket":"Lora", "metadata":{"data":"'+msg+'","node":"node 1"}}', hostname="api.mesosfer.com", port=1883, auth={'username':"lora", 'password':"03GlBhQTICzZNPi0"})
		elif(source==2):
			publish.single(topic, '{"appId":"pnsyDWDNrQ", "schemaId":"HZ6aV3ha7W", "bucket":"Lora", "metadata":{"data":"'+msg+'","node":"node 2"}}', hostname="api.mesosfer.com", port=1883, auth={'username':"lora", 'password':"03GlBhQTICzZNPi0"})
		else:
			publish.single(topic, '{"appId":"pnsyDWDNrQ", "schemaId":"HZ6aV3ha7W", "bucket":"Lora", "metadata":{"data":"'+msg+'","node":"node lain"}}', hostname="api.mesosfer.com", port=1883, auth={'username':"lora", 'password':"03GlBhQTICzZNPi0"})
		time.sleep(0.1)
		msg = "Hello back\0"
		print "Sending..."
		ret = rf95.sendtoWait(msg, len(msg), source)
		print "Sent!"
		time.sleep(0.05)
    	else:
        	time.sleep(0.05)

