#!/usr/bin/python

import sys, os
import paho.mqtt.publish as publish
# Add path to pyRadioHeadiRF95 module
sys.path.append(os.path.dirname(__file__) + "/../")

import pyRadioHeadRF95 as radio
import time

rf95 = radio.RF95()

rf95.init()

rf95.setTxPower(14, False)
rf95.setFrequency(868)

#rf95.setSignalBandwidth(rf95.Bandwidth500KHZ)
#rf95.setSpreadingFactor(rf95.SpreadingFactor12)
#rf95.setCodingRate4(rf95.CodingRate4_8)

print "StartUp Done!"
print "Receiving..."

numb = 0
topic = "device/lora/Lora Gateway"

while True:
    if rf95.available():
        numb += 1
        print "Available"
        (msg, l) = rf95.recv()
        print "Data #" + str(numb) + "\nReceived: " + msg + " (" + str(l) + ")"
        publish.single(topic, '{"appId":"pnsyDWDNrQ", "schemaId":"HZ6aV3ha7W", "bucket":"Lora", "metadata":{"data":"'+msg+'","node":"node 1"}}', hostname="api.mesosfer.com", port=1883, auth={'username':"lora", 'password':"03GlBhQTICzZNPi0"})
        time.sleep(0.5)
        msg = "Alive\0"
        print "Sending message..."
        rf95.send(msg, len(msg))
        rf95.waitPacketSent()
    else:
        time.sleep(0.05)
    
